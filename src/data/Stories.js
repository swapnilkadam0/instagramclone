export default [
  {
    user: {
      id: '1',
      imageUri: 'https://picsum.photos/200/300?random',
      name: 'Swapnil',
    },
    stories: [
      {
        imageUri:
          'https://i.insider.com/5d03aa8e6fc9201bc7002b43?width=1136&format=jpeg',
        postedTime: '25m',
      },
    ],
  },
  {
    user: {
      id: '2',
      imageUri: 'https://picsum.photos/200/300?random1',
      name: 'Dude',
    },
    stories: [
      {
        imageUri: 'https://picsum.photos/200/300?random50',
        postedTime: '30m',
      },
    ],
  },
  {
    user: {
      id: '3',
      imageUri: 'https://picsum.photos/200/300',
      name: 'Guy',
    },
    stories: [
      {
        imageUri:
          'https://i.insider.com/5d03aa8e6fc9201bc7002b43?width=1136&format=jpeg',
        postedTime: '30m',
      },
    ],
  },
  {
    user: {
      id: '4',
      imageUri: 'https://picsum.photos/200/300?random2',
      name: 'Jack',
    },
    stories: [
      {
        imageUri:
          'https://i.insider.com/5d03aa8e6fc9201bc7002b43?width=1136&format=jpeg',
        postedTime: '35m',
      },
    ],
  },
  {
    user: {
      id: '5',
      imageUri: 'https://picsum.photos/200/300?random3',
      name: 'Daniel',
    },
    stories: [
      {
        imageUri:
          'https://i.insider.com/5d03aa8e6fc9201bc7002b43?width=1136&format=jpeg',
        postedTime: '40m',
      },
    ],
  },
  {
    user: {
      id: '6',
      imageUri: 'https://picsum.photos/200/300?random4',
      name: 'David',
    },
    stories: [
      {
        imageUri:
          'https://i.insider.com/5d03aa8e6fc9201bc7002b43?width=1136&format=jpeg',
        postedTime: '45m',
      },
    ],
  },
  {
    user: {
      id: '7',
      imageUri: 'https://picsum.photos/200/300?random5',
      name: 'Raji',
    },
    stories: [
      {
        imageUri:
          'https://i.insider.com/5d03aa8e6fc9201bc7002b43?width=1136&format=jpeg',
        postedTime: '50m',
      },
    ],
  },
  {
    user: {
      id: '8',
      imageUri: 'https://picsum.photos/200/300?random6',
      name: 'Julio',
    },
    stories: [
      {
        imageUri:
          'https://i.insider.com/5d03aa8e6fc9201bc7002b43?width=1136&format=jpeg',
        postedTime: '55m',
      },
    ],
  },
  {
    user: {
      id: '9',
      imageUri: 'https://picsum.photos/200/300?random7',
      name: 'Jose',
    },
    stories: [
      {
        imageUri:
          'https://i.insider.com/5d03aa8e6fc9201bc7002b43?width=1136&format=jpeg',
        postedTime: '1hr',
      },
    ],
  },
  {
    user: {
      id: '10',
      imageUri: 'https://picsum.photos/200/300?random8',
      name: 'Bob',
    },
    stories: [
      {
        imageUri:
          'https://i.insider.com/5d03aa8e6fc9201bc7002b43?width=1136&format=jpeg',
        postedTime: '1hr 05m',
      },
    ],
  },
];
