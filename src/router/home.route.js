import {Image, View, StyleSheet} from 'react-native';

import React from 'react';
import MCIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Feather from 'react-native-vector-icons/Feather';

import Homescreen from '../screens/Homescreen';
import logo from '../assets/images/logo.png';
import {createStackNavigator} from '@react-navigation/stack';
import Colors from '../constants/Colors';

const HomeStack = createStackNavigator();

const HomeRoute = () => (
  <HomeStack.Navigator>
    <HomeStack.Screen
      name="Home"
      component={Homescreen}
      options={{
        title: 'InstagramClone',
        headerLeftContainerStyle: {
          padding: 0,
        },
        headerTitle: () => (
          // eslint-disable-next-line react-native/no-inline-styles
          <Image
            source={logo}
            resizeMode="contain"
            style={{width: 145, height: 50}}
          />
        ),
        headerRightContainerStyle: {
          marginRight: 0,
        },
        headerRight: () => (
          <View style={styles.headerIcon}>
            <Feather name="plus-square" size={25} color={Colors.PlusSquare} />
            <MCIcons
              name="facebook-messenger"
              size={25}
              color={Colors.FaceBookMsg}
            />
          </View>
        ),
      }}
    />
  </HomeStack.Navigator>
);
const styles = StyleSheet.create({
  headerIcon: {
    justifyContent: 'center',
    flexDirection: 'row',
    marginRight: 10,
  },
});

export default HomeRoute;
