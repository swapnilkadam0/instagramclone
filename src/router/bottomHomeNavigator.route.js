import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import Ionicons from 'react-native-vector-icons/Ionicons';
import Feather from 'react-native-vector-icons/Feather';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
//import MCIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import DiscoveryScreen from '../screens/DiscoverScreen';
import PostScreen from '../screens/PostScreen';
import NotificationsScreen from '../screens/NotificationsScreen';
import ProfileScreen from '../screens/ProfileScreen';
import HomeStackScreen from './home.route';
import Colors from '../constants/Colors';

const Tab = createBottomTabNavigator();

const BottomHomeNavigator = () => (
  <Tab.Navigator
    screenOptions={({route}) => ({
      tabBarIcon: ({focused, color, size}) => {
        let iconName;

        if (route.name === 'Home') {
          iconName = focused ? 'ios-home-outline' : 'ios-home-outline';
          return <Ionicons name={iconName} size={size} color={color} />;
        }
        if (route.name === 'Discover') {
          iconName = focused ? 'search-outline' : 'search-outline';
          return <Ionicons name={iconName} size={size} color={color} />;
        }
        if (route.name === 'Post') {
          iconName = focused ? 'plus-square' : 'plus-square';
          return <Feather name={iconName} size={size} color={color} />;
        }
        if (route.name === 'Notifications') {
          iconName = focused ? 'heart' : 'heart';
          return <EvilIcons name={iconName} size={35} color={color} />;
        }
        if (route.name === 'Profile') {
          iconName = focused ? 'ios-person-outline' : 'ios-person-outline';
          return <Ionicons name={iconName} size={size} color={color} />;
        }
      },
      tabBarActiveTintColor: Colors.tabBarActiveTint,
      tabBarInactiveTintColor: Colors.tabBarInActiveTint,
      tabBarShowLabel: false,
    })}>
    <Tab.Screen
      name="Home"
      component={HomeStackScreen}
      options={{headerShown: false}}
    />
    <Tab.Screen
      name="Discover"
      component={DiscoveryScreen}
      options={{headerShown: false}}
    />
    <Tab.Screen
      name="Post"
      component={PostScreen}
      options={{headerShown: false}}
    />
    <Tab.Screen
      name="Notifications"
      component={NotificationsScreen}
      options={{headerShown: false}}
    />
    <Tab.Screen
      name="Profile"
      component={ProfileScreen}
      options={{headerShown: false}}
    />
  </Tab.Navigator>
);

export default BottomHomeNavigator;
