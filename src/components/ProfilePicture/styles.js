import React from 'react';
import {StyleSheet} from 'react-native';
import Colors from '../../constants/Colors';

const styles = StyleSheet.create({
  container: {
    margin: 7,
    borderRadius: 40,
    borderWidth: 3,
    borderColor: Colors.Pink,
  },
  image: {
    borderRadius: 40,
    borderWidth: 1,
    borderColor: Colors.White,
  },
});
export default styles;
