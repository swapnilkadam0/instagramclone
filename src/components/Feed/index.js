import React from 'react';
import {FlatList} from 'react-native';
import Post from '../Post';
//import styles from '../ProfilePicture/styles';
import Stories from '../UserStoriesPreview';

const data = [
  {
    user: {
      imageUri: 'https://picsum.photos/200/300?random',
      name: 'Swapnil',
    },
    imageUri:
      'https://i.insider.com/5d03aa8e6fc9201bc7002b43?width=1136&format=jepg',
    caption: 'Beautiful City #instagram',
    likesCount: 1234,
    posted: '5 mins ago',
  },
  {
    user: {
      imageUri: 'https://picsum.photos/200/300',
      name: 'Dude',
    },
    imageUri: 'https://picsum.photos/200/300?random20',
    caption: 'Beautiful Landscape #instagram',
    likesCount: 2478,
    posted: '7 mins ago',
  },
  {
    user: {
      imageUri: 'https://picsum.photos/200/300?random2',
      name: 'Guy',
    },
    imageUri: 'https://picsum.photos/200/300?random22',
    caption: 'Road to Success #instagram',
    likesCount: 3584,
    posted: '10 mins ago',
  },
  {
    user: {
      imageUri: 'https://picsum.photos/200/300?random5',
      name: 'Raji',
    },
    imageUri: 'https://picsum.photos/200/300?random28',
    caption: 'Beautiful Beach #instagram',
    likesCount: 1234,
    posted: '15 mins ago',
  },
];

const Feed = () => (
  <FlatList
    data={data}
    renderItem={({item}) => <Post post={item} />}
    ListHeaderComponent={Stories}
  />
);
export default Feed;
