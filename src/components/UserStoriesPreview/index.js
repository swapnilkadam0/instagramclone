import React from 'react';
import {FlatList} from 'react-native';
import Story from '../UserStoryPreview';
import styles from './styles';
import StoriesData from '../../data/Stories';

// eslint-disable-next-line no-sparse-arrays

const UserStoriesPreview = () => (
  <FlatList
    style={styles.conatiner}
    data={StoriesData}
    horizontal
    showsHorizontalScrollIndicator={false}
    renderItem={({item}) => <Story story={item} />}
  />
);

export default UserStoriesPreview;
