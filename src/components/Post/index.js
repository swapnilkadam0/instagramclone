import React from 'react';

import Header from './comps/Header';
import Body from './comps/Body';
import Footer from './comps/Footer';
import {View} from 'react-native';

const Post = ({post}) => (
  <View>
    <Header imageUri={post.user.imageUri} name={post.user.name} />
    <Body imageUri={post.imageUri} />
    <Footer
      likesCount={post.likesCount}
      caption={post.caption}
      posted={post.posted}
    />
  </View>
);

export default Post;
