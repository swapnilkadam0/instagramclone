import {StyleSheet} from 'react-native';
import Colors from '../../../../constants/Colors';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  name: {
    alignSelf: 'center',
    fontWeight: 'bold',
    color: Colors.HeaderColor,
  },
  left: {
    flexDirection: 'row',
  },
  right: {
    marginRight: 7,
  },
});
export default styles;
