import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  likes: {
    fontWeight: 'bold',
    margin: 3,
  },
  captions: {
    margin: 3,
  },
  postedAt: {
    color: '#8c8c8c',
    margin: 3,
  },
  viewContainer: {
    margin: 5,
  },
  iconStyles: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 5,
    paddingHorizontal: 5,
    justifyContent: 'space-evenly',
  },
  symbolLayout: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});

export default styles;
