import React, {useState, useEffect} from 'react';
import {Text, View, TouchableNativeFeedback} from 'react-native';
import styles from './styles';
import Icicon from 'react-native-vector-icons/AntDesign';
import EIcon from 'react-native-vector-icons/EvilIcons';
import SLIcon from 'react-native-vector-icons/SimpleLineIcons';
import ICicon from 'react-native-vector-icons/Ionicons';
import Colors from '../../../../constants/Colors';

const Footer = ({likesCount: likesCountProp, caption, posted}) => {
  const [isLiked, setIsLiked] = useState(false);
  const [likesCount, setLikedCount] = useState(false);

  const onLikePressed = () => {
    //console.warn('On Like Pressed');
    const amount = isLiked ? -1 : 1;
    setLikedCount(likesCount + amount);
    setIsLiked(!isLiked);
  };
  useEffect(() => {
    setLikedCount(likesCountProp);
  }, []);

  return (
    <View style={styles.viewContainer}>
      <View style={styles.symbolLayout}>
        <View style={styles.iconStyles}>
          <TouchableNativeFeedback onPress={onLikePressed}>
            {isLiked ? (
              <Icicon name="heart" size={29} color={Colors.primaryColor} />
            ) : (
              <Icicon name="hearto" size={28} color={Colors.black} />
            )}
          </TouchableNativeFeedback>

          <EIcon name="comment" size={37} color={Colors.black} />
          <SLIcon name="paper-plane" size={26} color={Colors.black} />
        </View>
        <View>
          <ICicon name="ios-bookmark-outline" size={30} color={Colors.black} />
        </View>
      </View>
      <Text style={styles.likes}>{likesCount} Likes</Text>
      <Text style={styles.captions}>{caption}</Text>
      <Text style={styles.postedAt}>{posted}</Text>
    </View>
  );
};
export default Footer;
