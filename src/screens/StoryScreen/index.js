import React, {useEffect, useState} from 'react';

import {Image, SafeAreaView} from 'react-native';
import {PacmanIndicator} from 'react-native-indicators';
import {useRoute} from '@react-navigation/native';

import StoriesData from '../../data/Stories';
import styles from './styles';
import Colors from '../../constants/Colors';

const StoryScreen = () => {
  const [Stories, setUserStories] = useState(null);
  const [activeStoryIndex, setActiveStoryIndex] = useState(null);

  const route = useRoute();
  useEffect(() => {
    const userId = route.params.userId;
    // eslint-disable-next-line no-shadow
    const Stories = StoriesData.find(
      // eslint-disable-next-line no-shadow
      StoriesData => StoriesData.user.id === userId,
    );
    console.log(Stories);
    setUserStories(Stories);
    setActiveStoryIndex(Stories);
    setActiveStoryIndex(0);
  }, []);

  useEffect(() => {
    if (Stories && Stories.length > activeStoryIndex - 1) {
      setActiveStoryIndex(Stories.Stories[activeStoryIndex]);
    }
  }, [activeStoryIndex]);

  if (!Stories) {
    return <PacmanIndicator color={Colors.PackmanIndicatorColor} />;
  }

  return (
    <SafeAreaView style={styles.container}>
      <Image
        onError={() => console.log('Error')}
        source={{uri: Stories.stories[0].imageUri}}
        style={styles.image}
      />
    </SafeAreaView>
  );
};
export default StoryScreen;
