import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
  },
  image: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
  activityIndicator: {
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
  },
});

export default styles;
