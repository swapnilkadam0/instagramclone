import React from 'react';
import {View, Text} from 'react-native';
import styles from './styles';

const PostScreen = () => {
  return (
    <>
      <View style={styles.View}>
        <Text style={styles.Text}>PostScreen</Text>
      </View>
    </>
  );
};
export default PostScreen;
