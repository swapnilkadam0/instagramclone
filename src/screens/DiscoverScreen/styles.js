import {StyleSheet} from 'react-native';
import Colors from '../../constants/Colors';

const styles = StyleSheet.create({
  View: {
    backgroundColor: Colors.DiscoveryScreen,
    height: '100%',
  },
  Text: {
    textAlign: 'center',
    marginTop: 350,
    fontSize: 20,
    fontWeight: 'bold',
    fontStyle: 'italic',
    color: Colors.DiscoveryScreenText,
  },
});
export default styles;
