import React from 'react';
import Feed from '../../components/Feed';

const Homescreen = () => {
  return (
    <>
      <Feed />
    </>
  );
};

export default Homescreen;
