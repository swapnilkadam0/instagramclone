import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  header: {
    padding: 10,
    fontWeight: 'bold',
    fontSize: 15,
    fontFamily: 'MoonDance-Regular.ttf',
  },
  headerView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginRight: 10,
  },
  headerButton: {
    flexDirection: 'row',
    alignItems: 'space-between',
    justifyContent: 'center',
  },
});
export default styles;
