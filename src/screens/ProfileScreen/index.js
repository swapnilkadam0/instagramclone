import React from 'react';
import {View, Text} from 'react-native';
import styles from './styles';

const ProfileScreen = () => {
  return (
    <>
      <View style={styles.View}>
        <Text style={styles.Text}>ProfileScreen</Text>
      </View>
    </>
  );
};
export default ProfileScreen;
